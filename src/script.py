import os
import pandas as pd


def process_title_basics():
    data = pd.read_csv(os.path.join(os.getcwd(), 'data', 'title.basics.tsv'),sep='\t', na_values='\\N',
                       dtype={'tconst': 'string', 'titleType': 'string', 'primaryTitle': 'string',
                              'originalTitle': 'string', 'isAdult': pd.Int64Dtype(),
                              'startYear': pd.Int64Dtype(), 'endYear': pd.Int64Dtype(), 'runtimeMinutes': 'string', 'genres': 'string'})

    # titleTypes = ['short', 'movie', 'tvShort', 'tvMovie', 'tvSeries', 'tvEpisode', 'tvMiniSeries', 'tvSpecial', 'video', 'videoGame','tvPilot']
    titleTypesToExlude= ['tvEpisode', 'tvSpecial', 'video', 'videoGame', 'tvPilot']
    data = data[~data['titleType'].isin(titleTypesToExlude)]
    data['runtimeMinutes'] = pd.to_numeric(data['runtimeMinutes'])
    data = data.rename(columns={'tconst': 'titleId'})
    return data


def proccess_title_ratings():
    data = pd.read_csv(os.path.join(os.getcwd(), 'data', 'title.ratings.tsv'), sep='\t', na_values='\\N',
                       dtype={'tconst': 'string', 'taverageRating': pd.Float64Dtype(), 'tnumVotes': pd.Int64Dtype()})
    data = data.rename(columns={'tconst': 'titleId', 'taverageRating': 'averageRating', 'tnumVotes': 'numVotes'})
    return data


def process_episodes_seasons():
    data = pd.read_csv(os.path.join(os.getcwd(), 'data', 'title.episode.tsv'), sep='\t', na_values='\\N',
                       dtype={'tconst': 'string','parentTconst': 'string', 'seasonNumber': pd.Int64Dtype(), 'episodeNumber': pd.Int64Dtype()})
    data = data.rename(columns={'parentTconst': 'titleId'})
    data = data.groupby('titleId').agg(
        episodeCount=('episodeNumber', 'count'),
        seasonCount=('seasonNumber', 'max')
    ).reset_index()
    data['averageEpisodeCountPerSeason'] = data['episodeCount'] / data['seasonCount']
    return data


if __name__ == "__main__":
    pd.set_option('display.max_columns', None)
    pd.set_option('display.max_row', 20)

    basics = process_title_basics()
    ratings = proccess_title_ratings()
    data = basics.merge(ratings, on='titleId', how='left')
    episodes_seasons = process_episodes_seasons()
    data = data.merge(episodes_seasons, on='titleId', how='left')
    data = data.dropna(subset=['averageRating', 'runtimeMinutes', 'numVotes'])
    data.to_csv('output/data.csv', index=False)

